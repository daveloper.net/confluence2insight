#!/usr/bin/python3

# Imports
import requests
from requests.structures import CaseInsensitiveDict
import json
import ast # Library to change text to dictionary

    # Declare bearer token variable
global bearerToken
bearerToken = ''

    # Declare header variable
global my_headers
my_headers = {"Authorization" : f"Bearer {bearerToken}"}

global headers
headers = CaseInsensitiveDict()

    # headers["Accept"] = "*/*"
headers["Content-Type"] = "application/json"
headers["Authorization"] = f"Bearer {bearerToken}"

#-- GET REQUEST --
def GETReq():
        # Declare variable for the URL
    url = "https://api.atlassian.com/jsm/insight/v1/imports/info"


    def jprint(obj):
        text = json.dumps(obj, sort_keys=True, indent=4)
        jprint.responseURLS = json.dumps(obj)
        #print(jprint.responseURLS)

        
        # GET Request
    responseGET = requests.get(url, headers=headers)

    jprint(responseGET.json())
    
    jprint.responseURLS = ast.literal_eval(json.dumps(responseGET.json()))

    getStatusURL = jprint.responseURLS['links']['getStatus']
    startURL = jprint.responseURLS['links']['start']
    mappingURL = jprint.responseURLS['links']['mapping']
        
    print('Get Status Code: ', responseGET.status_code)
    #print('\ngetStatusURL: ', getStatusURL)
    #print('\nstartURL: ', startURL)
    #print('\nmappingURL: ', mappingURL)

    return mappingURL, startURL




#-- PUT REQUEST --
def PUTReq(schemaMapping, URL):
    with open(schemaMapping, 'r') as PUTFile:
        PUTSchema = json.load(PUTFile)

        # PUT Mapping Request
    responseMap = requests.put(URL, json=PUTSchema, headers=headers)

        #Print GET, getstatus, map responses
    print('PUT Request status code: ', responseMap.status_code)


#-- StartPOST --
def startPOST(sURL):
        # Send Post Request    
    responsePOST = requests.post(sURL, headers=headers)

        # Assign URLS to variables
    startResponseURLS = ast.literal_eval(json.dumps(responsePOST.json()))
    #print('StartResponseURLS: ', startResponseURLS)
    
    submitProgressURL = startResponseURLS['links']['submitProgress']
    submitResultsURL = startResponseURLS['links']['submitResults']
    getExecutionStatusURL = startResponseURLS['links']['getExecutionStatus']
    cancelURL = startResponseURLS['links']['cancel']

    print('POST start URL request status code: ', responsePOST.status_code)

    return submitResultsURL


#-- Submit Data and Completed Request --
def submitCompleted(jsonDataFile, srURL):
    
    with open(jsonDataFile, 'r') as POSTDataFile:
        POSTData = json.load(POSTDataFile)

    #with open('data_json_template_test1.json') as data_file:
        #JSONDataString = data_file.read()
        #Convert string into JSON
    #data = json.loads(JSONDataString)
    #jsonDtaFile = json.loads(jsonDataFile)
    #print(jsonDataFile)
        # POST Import Request
    responseImport = requests.post(srURL, json=POSTData, headers=headers) # POSTData

        # Print POST import status code
    print('Data Import POST status code: ', responseImport.status_code)

   
    #JSONDataString = json.loads(jsonDataFile)

 
#-- Main function --
def main():

    mappingURL, startURL = GETReq()
    #print('\nstartURL: ', startURL)

    schemaMappingJSON = 'snipeit_schemaMapping_json.json'
    PUTReq(schemaMappingJSON, mappingURL)

    submitResultsURL = startPOST(startURL)
    #print('\nsubmitResultsURL: ', submitResultsURL)

    snipeitData = 'manufacturers_data.json'    
    submitCompleted(snipeitData, submitResultsURL)






if __name__ == "__main__":
    main()
