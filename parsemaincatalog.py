#!/usr/bin/python3

# Name: (Dave Loper)
# Class: (INFO 2200)
# Section: (X02)
# Professor: (Crandall)
# Date: 28 April 2022
# Project #: Honors Contract: Confluence to Insight webpage to JSON driver

# I declare that the source code contained in this file was written solely by me.
# Copyright 2022, AGPL 3.0+ See https://www.gnu.org/licenses/agpl-3.0.en.html

import re   # We need the regex library to identify lines in our source docs
import io   # We also need to interact with the filesystem to open docs
import json # We need to render our output into JSON. We can use this library

CATALOGFILENAME="doc.html"              # the import file
OUTPUTFILENAME="servicecatalog.json"    # the output file
lines = []                              # initialize this array
debug=0                                 # Set to 1 for debug options
jsonString=""                           # initialize the string variable for the output

class ConfluenceImport():               # Eventually we will want to use libraries from this program as a class

    def open_catalog():                                     # This function pens our file and returns the mega line
        global CATALOGFILENAME                              # our input file from variable
        returnline = ""                                     # our return variable for this function
        with io.open(CATALOGFILENAME, encoding="utf-8") as file: # Use file to refer to the file object
            data = file.readlines()                         # read our lines from the file
            for line in data:                                   # iterate over the lines
                if (re.search("Business Service", line)):       # regex for the the table content (should set this via input in the future)
                    returnline += line                          # add this to the return data
        return(returnline)                                  # return our data

    def write_file():                                                           # This function writes our of json string to a file
        global jsonString                                                       # use the global variable
        if jsonString == "":                                                    # if it is empty, don't do anything
            print("No data available to write. Try parsing the file first.")    # except print out the error to console
        else:                                                                   # if we have data, let's write it. Future: should also test valid structure
            with io.open(OUTPUTFILENAME, mode="a+", encoding="utf-8") as file:  # open the file for writing
                file.write(jsonString)                                          # write out our data to the file

    def parse_lines():                                              # Core of the program, this is the parsing engine
        servicelist = {}                                            # Setup  our dictionary
        line=ConfluenceImport.open_catalog()                        # call the open_catalog function to pull the line data
        lines=line.split("><")                                      # split the data into a large array
        counter1=0                                                  # This counter is on when we are on the start of a new line
        counter3=0                                                  # This counter ...
        counter2=0                                                  # This counter ...
        for i in lines:                                                 # Iterate over the lines
            if (re.search("^a*.*https://uvu-it.atlassian.net/wiki/spaces/ITDOC/pages*.*a$", i)):    # Conditional based on regex
                j=re.split('a href=\"|\">|</a',i)                                                   # on matches lines, split
                counter1=1                                                                          # We are on a new row
                newarrayline = {"Business Service":j[2], "URL":j[1]}                                # Set the dictionary definition and the first element
                counter3=1                                                                          # Counter for the start of a column
            if (re.search("^p*.*http://www.w3.org/1999/xhtml", i)):             # Conditional based on regex of secondary element (2nd column or more)
                if (re.search("p xmlns=\"http://www.w3.org/1999/xhtml\"$",i)):  # If the column is totally blank, fill in the blanks so we don't abort
                    j=['','','']                                                # there are 3 columns
                else:                                                           # otherwise we have data... 
                    j=re.split('www.w3.org/1999/xhtml\">|</p$',i)               # ...and need to split the line.
                if (counter1 == 1):                                     # If we are on a new line
                    if (counter3 == 1):                                 # And if the cell is the first cell
                        newarrayline['Primary Contact']=j[1]            # set array element
                    elif (counter3 == 2):                               # And if the cell is the second cell
                        newarrayline['PCM']=j[1]                        # set array element
                    elif (counter3 == 3):                               # And if the cell is the third cell
                        newarrayline['Department']=j[1]                 # set array element
                    elif (counter3 == 4):                               # And if the cell is the fourth cell
                       newarrayline['Brief Description']=j[1]           # set array element
                    elif (counter3 == 5):                               # And if the cell is the fifth cell
                        newarrayline['Last Revision Date']=j[1]         # set array element
                    elif (counter3 == 6):                               # And if the cell is the sixth cell
                        newarrayline['BS #']=j[1]                       # set array element
                    if (counter3 == 6):                                 # Also do this for last cell
                        if debug == 1:              # Debug option
                            print(newarrayline)     # print what we got
                        servicelist[counter2] = newarrayline            # set the full row
                        counter2+=1                                     # increment the row counter
                    counter3+=1                                         # increment the column counter (note: reset for new rows elsewhere)
        jsonString = json.dumps(servicelist, indent=4)      # Reformat array in JSON
        return jsonString                                   # Return our JSON string

    # Future implementation
    def output_urls():  # Future report (not implemented yet)
        pass            # Future report (not implemented yet)

    # A helpful display
    def display_menu(): # Display our help menu
        print("help, parse, print, write, quit")    # Output commands for main function

    # Our main function
    def main():                                             # Our main menu
        global lines                                        # globally use our line variable
        global jsonString                                   # globally use the json String
        print()                                             # print an extra line
        ConfluenceImport.display_menu()                     # call the display menu
        while True:                                         # do this over and over
            command = ""                                    # set default command to blank
            command = input("Specify a command: ")          # prompt for input
            print()                                         # give a blank line
            if (command == "parse"):                        # do this if the user wants to part the data
                jsonString=ConfluenceImport.parse_lines()   # run the parselines function
            elif command == "print":                        # print out our data conditional
                if jsonString == "":                        # if we have it
                    print("No JSON data available, try running parse")  # and it is not blank
                else:                                       # otherwise
                    print(jsonString)                       # do the function's purpose and print
            elif command == "help":                         # our help menu
                ConfluenceImport.display_menu()             # call the display function
            elif (command == "write"):                      # write the file out to disk function
                 ConfluenceImport.write_file()              # call the write function
            elif (command == "exit") or (command == "quit"):# the "let's get out of here" function
                break                                       # break out of 'while True'
            else:                                           # garbage in garbage out function for everything else
                print("Not a valid command. Please try again.\n")   # print out help message
        print("Bye!")                                       # we broke, so we leave

# Call the main function
if __name__ == "__main__":      # A common way to start a program
    ConfluenceImport.main()     # A call to our main function, let's get started

